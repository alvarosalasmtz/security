package com.devsarts.security.components

import com.devsarts.security.models.AuthorizationCode
import com.devsarts.security.repositorys.AuthorizationCodeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.code.RandomValueAuthorizationCodeServices
import org.springframework.stereotype.Component

import static com.devsarts.security.utils.SerializableUtil.deserialize
import static com.devsarts.security.utils.SerializableUtil.serialize

@Component
class AuthorizationCodeComponent extends RandomValueAuthorizationCodeServices {

    @Autowired
    AuthorizationCodeRepository authorizationCodeRepository

    @Override
    protected void store(String code, OAuth2Authentication authentication) {
        authorizationCodeRepository.save(new AuthorizationCode(code: code, authentication: serialize(authentication)))
    }

    @Override
    protected OAuth2Authentication remove(String code) {
        OAuth2Authentication authentication = null
        authorizationCodeRepository.findById(code).ifPresent { a11nCode ->
            authentication = deserialize(a11nCode.authentication)
        }
        authentication
    }
}
