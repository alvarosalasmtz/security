package com.devsarts.security.components

import com.devsarts.security.models.AccessToken
import com.devsarts.security.repositorys.AccessTokenRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.common.OAuth2RefreshToken
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.token.AuthenticationKeyGenerator
import org.springframework.security.oauth2.provider.token.DefaultAuthenticationKeyGenerator
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.stereotype.Component

import static com.devsarts.security.utils.SerializableUtil.deserialize
import static com.devsarts.security.utils.SerializableUtil.serialize

@Component
class TokenStoreComponent implements TokenStore {

    private AuthenticationKeyGenerator authenticationKeyGenerator = new DefaultAuthenticationKeyGenerator()

    @Autowired
    AccessTokenRepository accessTokenRepository

    @Override
    OAuth2Authentication readAuthentication(OAuth2AccessToken accessToken) {
        return readAuthentication(accessToken.getValue())
    }

    @Override
    OAuth2Authentication readAuthentication(String token) {
        OAuth2Authentication authentication = null
        accessTokenRepository.findById(token).ifPresent({ accessToken ->
            authentication = deserialize(accessToken.authentication)
        })
        authentication
    }

    @Override
    void storeAccessToken(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        removeAccessToken(accessToken)
        Map props = accessToken.properties
        props.remove("class")
        AccessToken token = new AccessToken(props)
        token.authenticationId = authenticationKeyGenerator.extractKey(authentication)
        token.authentication = serialize(authentication)
        token.username = authentication.isClientOnly() ? null : authentication.getName()
        token.clientId = authentication.getOAuth2Request().getClientId()
        accessTokenRepository.save(token)
    }

    @Override
    OAuth2AccessToken readAccessToken(String tokenValue) {
        accessTokenRepository.findById(tokenValue).ifPresent({ token ->
            return token
        })
        null
    }

    @Override
    void removeAccessToken(OAuth2AccessToken oAuth2AccessToken) {
        if (accessTokenRepository.existsById(oAuth2AccessToken.getValue())) {
            accessTokenRepository.deleteById(oAuth2AccessToken.getValue())
        }
    }

    @Override
    void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
        String authenticationId = authenticationKeyGenerator.extractKey(authentication)
        accessTokenRepository.findOneByAuthenticationId(authenticationId).ifPresent({ accessToken ->
            accessToken.refreshToken = refreshToken
            accessTokenRepository.save(accessToken)
        })
    }

    @Override
    OAuth2RefreshToken readRefreshToken(String tokenValue) {
        accessTokenRepository.findById(tokenValue) ifPresent({ accessToken ->
            return accessToken.refreshToken
        })
        null
    }

    @Override
    OAuth2Authentication readAuthenticationForRefreshToken(OAuth2RefreshToken refreshToken) {
        accessTokenRepository.findOneByRefreshToken(refreshToken).ifPresent({ token ->
            return deserialize(token.authentication)
        })
        null
    }

    @Override
    void removeRefreshToken(OAuth2RefreshToken refreshToken) {
    }

    @Override
    void removeAccessTokenUsingRefreshToken(OAuth2RefreshToken refreshToken) {
        accessTokenRepository.findOneByRefreshToken(refreshToken).ifPresent({ accessToken ->
            accessTokenRepository.delete(accessToken)
        })
    }

    @Override
    OAuth2AccessToken getAccessToken(OAuth2Authentication authentication) {
        String authenticationId = authenticationKeyGenerator.extractKey(authentication)
        OAuth2AccessToken oAuth2AccessToken = null
        accessTokenRepository.findOneByAuthenticationId(authenticationId).ifPresent({ accessToken ->
            oAuth2AccessToken = accessToken
            if (oAuth2AccessToken != null && !authenticationId.equals(
                    authenticationKeyGenerator.extractKey(readAuthentication(oAuth2AccessToken)))) {
                removeAccessToken(oAuth2AccessToken)
                storeAccessToken(oAuth2AccessToken, authentication)
            }
        })
        return oAuth2AccessToken
    }

    @Override
    Collection<OAuth2AccessToken> findTokensByClientIdAndUserName(String clientId, String username) {
        Collection<OAuth2AccessToken> tokens = new ArrayList<OAuth2AccessToken>()
        accessTokenRepository.findByClientIdAndUsername(clientId, username).parallelStream().forEach({ accessToken ->
            tokens.add(accessToken)
        })
        tokens
    }

    @Override
    Collection<OAuth2AccessToken> findTokensByClientId(String clientId) {
        Collection<OAuth2AccessToken> tokens = new ArrayList<OAuth2AccessToken>()
        accessTokenRepository.findByClientId(clientId).parallelStream().forEach({ accessToken ->
            tokens.add(accessToken)
        })
        tokens
    }
}
