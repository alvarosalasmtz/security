package com.devsarts.security.components

import com.devsarts.security.models.ClientDetailsModel
import com.devsarts.security.repositorys.ClientDetailsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.oauth2.provider.*
import org.springframework.stereotype.Component

import static org.springframework.security.crypto.password.NoOpPasswordEncoder.getInstance

@Component
class ClientDetailsComponent implements ClientDetailsService, ClientRegistrationService {

    @Autowired
    ClientDetailsRepository clientDetailsRepository

    @Override
    ClientDetailsModel loadClientByClientId(String clientId) throws ClientRegistrationException {
        Optional<ClientDetailsModel> optional = clientDetailsRepository.findById(clientId)
        if (!optional.isPresent()) {
            throw new ClientRegistrationException("Client with id ${clientId} not found")
        }
        return optional.get()
    }

    @Override
    void addClientDetails(ClientDetails clientDetails) throws ClientAlreadyExistsException {
        if (clientDetailsRepository.existsById(clientDetails.clientId)) {
            throw new ClientAlreadyExistsException("Client with id ${clientDetails.getClientId()} already existed")
        }
        ClientDetailsModel client = new ClientDetailsModel(clientDetails)
        client.clientSecret = getInstance().encode(clientDetails.getClientSecret())
        client.autoApprove = true
        clientDetailsRepository.save(client)
    }

    @Override
    void updateClientDetails(ClientDetails clientDetails) throws NoSuchClientException {
        ClientDetailsModel client = findById(clientDetails.clientId)
        clientDetails.getProperties().forEach({ key, value ->
            client."${key}" = value
        })
        clientDetailsRepository.save(client)
    }

    @Override
    void updateClientSecret(String clientId, String secret) throws NoSuchClientException {
        ClientDetailsModel client = findById(clientId)
        client.clientSecret = getInstance().encode(secret)
        clientDetailsRepository.save(client)
    }

    @Override
    void removeClientDetails(String clientId) throws NoSuchClientException {
        clientDetailsRepository.deleteById(clientId)
    }

    @Override
    List<ClientDetails> listClientDetails() {
        clientDetailsRepository.findAll()
    }

    private ClientDetailsModel findById(String id) throws NoSuchClientException {
        try {
            return loadClientByClientId(id)
        } catch (ClientRegistrationException e) {
            throw new NoSuchClientException(e.getMessage(), e)
        }
    }
}
