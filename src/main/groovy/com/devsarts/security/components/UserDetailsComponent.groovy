package com.devsarts.security.components

import com.devsarts.security.models.User
import com.devsarts.security.repositorys.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component

@Component
class UserDetailsComponent implements UserDetailsService {

    @Autowired
    UserRepository userRepository

    @Override
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optional = userRepository.findById(username)
        if (!optional.isPresent()) {
            throw new UsernameNotFoundException("Username ${username} not found")
        }
        optional.get()
    }
}
