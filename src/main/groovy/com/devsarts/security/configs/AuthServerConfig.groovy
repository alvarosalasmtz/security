package com.devsarts.security.configs

import com.devsarts.security.components.AuthorizationCodeComponent
import com.devsarts.security.components.TokenStoreComponent
import com.devsarts.security.components.ClientDetailsComponent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.Authentication
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.token.DefaultTokenServices
import org.springframework.security.oauth2.provider.token.TokenEnhancer
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter

@Configuration
@EnableAuthorizationServer
class AuthServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private AuthenticationManager authenticationManager
    @Autowired
    private TokenStoreComponent tokenStoreComponent
    @Autowired(required = false)
    private JwtAccessTokenConverter accessTokenConverter
    @Autowired
    private AuthorizationCodeComponent authorizationCodeComponent
    @Autowired
    private ClientDetailsComponent clientDetailsComponent

    @Override
    void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.withClientDetails(clientDetailsComponent)
    }

    @Override
    void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.authorizationCodeServices(authorizationCodeComponent)
                .tokenServices(tokenServices())
                .authenticationManager(authenticationManager)
    }

    @Override
    void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()")
        oauthServer.allowFormAuthenticationForClients()
    }

    @Primary
    @Bean
    DefaultTokenServices tokenServices() {
        DefaultTokenServices tokenServices = new DefaultTokenServices()
        tokenServices.setSupportRefreshToken(true)
        tokenServices.setTokenStore(tokenStoreComponent)

        List<TokenEnhancer> enhancers = new ArrayList<>()
        if (accessTokenConverter != null) {
            enhancers.add(accessTokenConverter)
        }

        //Some custom enhancer
        enhancers.add(new TokenEnhancer() {
            @Override
            OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
                final Authentication userAuthentication = authentication.getUserAuthentication()

                final DefaultOAuth2AccessToken defaultOAuth2AccessToken = (DefaultOAuth2AccessToken) accessToken
                Set<String> existingScopes = new HashSet<>()
                existingScopes.addAll(defaultOAuth2AccessToken.getScope())
                if (userAuthentication != null) {
                    //User has logged into system
                    existingScopes.add("read-foo")
                } else {
                    //service is trying to access system
                    existingScopes.add("another-scope")
                }

                defaultOAuth2AccessToken.setScope(existingScopes)
                return defaultOAuth2AccessToken
            }
        })

        TokenEnhancerChain enhancerChain = new TokenEnhancerChain()
        enhancerChain.setTokenEnhancers(enhancers)
        tokenServices.setTokenEnhancer(enhancerChain)
        return tokenServices
    }
}
