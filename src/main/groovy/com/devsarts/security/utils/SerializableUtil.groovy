package com.devsarts.security.utils

import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.util.SerializationUtils

import org.apache.commons.codec.binary.Base64

final class SerializableUtil {

    private SerializableUtil() {}

    static String serialize(OAuth2Authentication object) {
        Base64.encodeBase64String(SerializationUtils.serialize(object))
    }

    static OAuth2Authentication deserialize(String encodedObject) {
        (OAuth2Authentication) SerializationUtils.deserialize(Base64.decodeBase64(encodedObject))
    }
}
