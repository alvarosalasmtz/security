package com.devsarts.security

import com.devsarts.security.models.ClientDetailsModel
import com.devsarts.security.models.Rol
import com.devsarts.security.models.User
import com.devsarts.security.repositorys.ClientDetailsRepository
import com.devsarts.security.repositorys.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.security.crypto.password.PasswordEncoder

@SpringBootApplication
class Application {

    @Autowired
    private UserRepository userRepository

    @Autowired
    private ClientDetailsRepository clientDetailsRepository

    @Autowired
    private PasswordEncoder passwordEncoder

    static void main(String[] args) {
        SpringApplication.run(Application.class, args)
    }

    @Bean
    boolean init() {
        userRepository.deleteAll()
        clientDetailsRepository.deleteAll()

        // init the users
        User user = new User(
                username: "user",
                password: passwordEncoder.encode("user"),
                authorities: [new Rol(authority: "ROLE_USER")],
                credentialsNonExpired: true,
                accountNonLocked: true,
                accountNonExpired: true,
                enabled: true
        )
        userRepository.save(user)

        // init the client details
        ClientDetailsModel clientDetails = new ClientDetailsModel()
        clientDetails.setClientId("web-client")
        clientDetails.setClientSecret(passwordEncoder.encode("web-client-secret"))
        clientDetails.setSecretRequired(true)
        clientDetails.resourceIds = ["foo"]
        clientDetails.scope = ["read-foo"]
        clientDetails.authorizedGrantTypes = ["authorization_code", "refresh_token", "password", "client_credentials"]
        clientDetails.registeredRedirectUri = ["http://localhost:8082/resource-service"]
        clientDetails.setAuthorities([new Rol(authority: "ROLE_USER")])
        clientDetails.setAccessTokenValiditySeconds(60)
        clientDetails.setRefreshTokenValiditySeconds(14400)
        clientDetails.setAutoApprove(false)
        clientDetailsRepository.save(clientDetails)

        true
    }
}
