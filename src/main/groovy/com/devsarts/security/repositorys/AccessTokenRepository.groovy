package com.devsarts.security.repositorys

import com.devsarts.security.models.AccessToken
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.security.oauth2.common.OAuth2RefreshToken

interface AccessTokenRepository extends MongoRepository<AccessToken, String> {
    Optional<AccessToken> findOneByRefreshToken(String refreshToken)

    Optional<AccessToken> findOneByAuthenticationId(String authenticationId)

    Optional<AccessToken> findOneByRefreshToken(OAuth2RefreshToken refreshToken)

    List<AccessToken> findByClientId(String clientId)

    List<AccessToken> findByClientIdAndUsername(String clientId, String username)
}
