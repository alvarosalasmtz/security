package com.devsarts.security.repositorys

import com.devsarts.security.models.ClientDetailsModel
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface ClientDetailsRepository extends MongoRepository<ClientDetailsModel, String> {

}