package com.devsarts.security.repositorys

import com.devsarts.security.models.User
import org.springframework.data.mongodb.repository.MongoRepository

interface UserRepository extends MongoRepository<User, String> {

}