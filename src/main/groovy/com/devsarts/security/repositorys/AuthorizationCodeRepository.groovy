package com.devsarts.security.repositorys

import com.devsarts.security.models.AuthorizationCode
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface AuthorizationCodeRepository extends MongoRepository<AuthorizationCode, String> {

}
