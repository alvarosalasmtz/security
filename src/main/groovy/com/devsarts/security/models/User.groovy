package com.devsarts.security.models

import org.springframework.data.annotation.Id
import org.springframework.security.core.userdetails.UserDetails

class User implements UserDetails {

    @Id
    String username
    String password
    Collection<Rol> authorities
    boolean accountNonExpired
    boolean accountNonLocked
    boolean credentialsNonExpired
    boolean enabled

    @Override
    Collection<Rol> getAuthorities() {
        return authorities
    }

    @Override
    boolean isAccountNonExpired() {
        return accountNonExpired
    }

    @Override
    boolean isAccountNonLocked() {
        return accountNonLocked
    }

    @Override
    boolean isCredentialsNonExpired() {
        return credentialsNonExpired
    }

    @Override
    boolean isEnabled() {
        return enabled
    }
}
