package com.devsarts.security.models

import org.springframework.data.annotation.Id

class AuthorizationCode {

    @Id
    String code
    String authentication
}
