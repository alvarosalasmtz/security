package com.devsarts.security.models

import org.springframework.data.annotation.Id
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.common.OAuth2RefreshToken

class AccessToken implements Serializable, OAuth2AccessToken {

    private static final long serialVersionUID = 914967629530462926L

    @Id
    String value
    String authenticationId
    String username
    String clientId
    String authentication
    Date expiration
    String tokenType = BEARER_TYPE.toLowerCase()
    OAuth2RefreshToken refreshToken
    Set<String> scope
    Map<String, Object> additionalInformation = Collections.emptyMap()
    boolean expired
    int expiresIn

    @Override
    int getExpiresIn() {
        expiresIn
    }

    @Override
    boolean isExpired() {
        return expiration?.before(new Date())
    }

    @Override
    Date getExpiration() {
        expiration
    }

    @Override
    Map<String, Object> getAdditionalInformation() {
        additionalInformation
    }

    @Override
    Set<String> getScope() {
        scope
    }

    @Override
    OAuth2RefreshToken getRefreshToken() {
        refreshToken
    }

    @Override
    String getTokenType() {
        tokenType
    }
}
