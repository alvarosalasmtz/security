package com.devsarts.security.models

import org.springframework.security.core.GrantedAuthority

class Rol implements GrantedAuthority {

    String authority

    @Override
    String getAuthority() {
        return authority
    }
}
