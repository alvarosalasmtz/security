package com.devsarts.security.models

import org.springframework.data.annotation.Id
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.oauth2.provider.ClientDetails

class ClientDetailsModel implements ClientDetails {

    @Id
    String clientId
    Set<String> resourceIds
    boolean secretRequired
    String clientSecret
    boolean scoped
    Set<String> scope
    Set<String> authorizedGrantTypes
    Set<String> registeredRedirectUri
    Collection<GrantedAuthority> authorities
    Integer accessTokenValiditySeconds
    Integer refreshTokenValiditySeconds
    boolean autoApprove
    Map<String, Object> additionalInformation

    @Override
    String getClientId() {
        clientId
    }

    @Override
    Set<String> getResourceIds() {
        resourceIds
    }

    @Override
    boolean isSecretRequired() {
        secretRequired
    }

    @Override
    String getClientSecret() {
        clientSecret
    }

    @Override
    boolean isScoped() {
        scoped
    }

    @Override
    Set<String> getScope() {
        scope
    }

    @Override
    Set<String> getAuthorizedGrantTypes() {
        authorizedGrantTypes
    }

    @Override
    Set<String> getRegisteredRedirectUri() {
        registeredRedirectUri
    }

    @Override
    Collection<GrantedAuthority> getAuthorities() {
        authorities
    }

    @Override
    Integer getAccessTokenValiditySeconds() {
        accessTokenValiditySeconds
    }

    @Override
    Integer getRefreshTokenValiditySeconds() {
        refreshTokenValiditySeconds
    }

    @Override
    boolean isAutoApprove(String scope) {
        autoApprove
    }

    @Override
    Map<String, Object> getAdditionalInformation() {
        additionalInformation
    }
}
